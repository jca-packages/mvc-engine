<?php
	declare(strict_types=1);

	use Jca\Engine\App;
	use PHPUnit\Framework\TestCase;

	/**
	 * App test class
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class AppTest extends TestCase
	{
		/**
		 * App test
		 * @method testApp
		 */
		public function testApp(): void
		{
			$_SERVER["REQUEST_URI"] = "test";

			// App::instance()->run();
			$this->assertTrue(true);
		}
	}

?>
