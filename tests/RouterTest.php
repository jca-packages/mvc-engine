<?php
	declare(strict_types=1);

	use Jca\Engine\Exceptions\HTTPException;
	use Jca\Engine\State\Router;
	use PHPUnit\Framework\TestCase;

	/**
	 * Router test class
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class RouterTest extends TestCase
	{
		/**
		 * Router error test
		 * @method testRouterError
		 */
		public function testRouterError(): void
		{
			$_SERVER["REQUEST_URI"] = "test";

			try
			{
				// Router test
				$router = new Router($_SERVER["REQUEST_URI"]);
				$router->process();

				$router->run();
				$this->assertTrue(false);
			}
			catch(HTTPException $e)
			{
				$this->assertEquals($e->getHTTPCode(), 404);
			}
		}
	}
?>
