<?php
	namespace Jca\Engine\MVC;

	use Jca\Engine\App;

	/**
	 * View description class
	 */
	class View
	{
		private $layout = 'close';

		private $path;

		private $model;

		private $scripts = [];

		private $styles = [];

		public function __construct($path)
		{
			$this->path = $path;// . ".php";

			if(key_exists('default-layout', App::instance()->getConfig('app')))
				$this->layout = App::instance()->getConfig('app')['default-layout'];
		}

		public function addScript($url)
		{
			$this->scripts[] = $url;
		}

		public function addStyle($url, $prefix ='')
		{
			$this->styles[] = $prefix.$url;
		}

		public function render()
		{
			// Content
			ob_start();
			require $this->path; // . '.php';
			$content = ob_get_clean();

			// Layout
			ob_start();
			if(file_exists('../views/layouts/' . $this->layout . '.php'))
				require '../views/layouts/' . $this->layout . '.php';
			echo ob_get_clean();
		}

		public function renderSimple()
		{
			// Content
			ob_start();
			require '../views/' . $this->path . '.php';
			echo ob_get_clean();
		}

		public function setLayout($layout)
		{
			$this->layout = $layout;
		}

		public function setModel($model)
		{
			$this->model = $model;
		}
	}
?>
