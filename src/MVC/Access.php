<?php
	namespace Jca\Engine\MVC;

	use Jca\Automodel\Automodel;
	use Jca\Automodel\Utils\NameUtils;
	use Jca\Engine\App;
	use Jca\Engine\Exceptions\HTTPException;
	use Jca\Engine\MVC\View;

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	/**
     * Access attribute
     */
    #[\Attribute(\Attribute::TARGET_METHOD)]
	class Access
	{
        public const ACCESS_GUEST = 0;
        public const ACCESS_EDITOR = 1;
        public const ACCESS_ADMIN = 2;

        public function __construct(public int $right) {}
    }
?>
