<?php
	namespace Jca\Engine\MVC;

	use Jca\Automodel\Automodel;
	use Jca\Automodel\Utils\NameUtils;
	use Jca\Engine\App;
	use Jca\Engine\Exceptions\HTTPException;
	use Jca\Engine\MVC\View;

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	/**
	 * Controller class
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class Controller
	{
		public static $default_layout = 'container';

		protected $access = [];

		/**
		 * Working model
		 * @var object
		 */
		protected $model;

		public static function staticGotoCreate($className)
		{
			global $model;
			$model = new Automodel(NameUtils::classNameToTableName($className));

			// Model fill
			$post = $_POST;
			if(!empty($post))
			{
				$model->fill($post);

				if($model->validate($post) && $model->save())
				{
					header('Location: ' .  'view?id=' . $model->id , true, 301);
    				exit();
				}
			}

			$view = new View(self::getDefaultPath('create'));
			$view->setModel($model);
			$view->render();
		}

		public static function getDefaultPath($viewName)
		{
			$default_path = '../views/_default/' . $viewName . '.php';
			$framework_path = __DIR__ . '/../default/views/' . $viewName . '.php';

			if(file_exists($default_path))
				return $default_path;
			if(file_exists($framework_path))
				return $framework_path;
			throw new HTTPException(404);
		}
		public static function staticGotoIndex($className)
		{
			global $model;
			$entityName = NameUtils::classNameToEntityName($className);

			$method_name = 'find' . ucfirst(NameUtils::classNameToTableName($className));

			$model = [];
			if(isset($_SESSION) && array_key_exists('user', $_SESSION))
			{
				$model = Automodel::$method_name($_SESSION['user']->id);
				if(!is_array($model))
					$model = [$model];
			}

			$path = '../views/' . $entityName . '/index.php';
			
			$view = new View(file_exists($path) ? $path : self::getDefaultPath('index'));
			$view->setModel($model);
			$view->render();
		}

		public static function staticGotoView($className)
		{
			$id = $_GET['id'];

			global $model;
			global $entityName;

			$methodName = 'find' . ucfirst($className) . 'ById';
			$model = Automodel::$methodName($id)[0];

			$entityName = $className;

			$path = '../views/' . $entityName . '/view.php';
			
			$view = new View(file_exists($path) ? $path : self::getDefaultPath('view'));
			
			//$view = new View(self::getDefaultPath('view'));

			$view->setModel($model);
			$view->render();
		}

		public static function staticGotoDelete($className)
		{
			$id = $_GET['id'];
			$method_name = 'find' . ucfirst(NameUtils::classNameToTableName($className)) . 'ById';
			$model = Automodel::$method_name($id)[0];
			$model->delete();

			self::redirect("/$className/index");
		}

		public static function goto($entityName, $viewName)
		{
			$view = new View($entityName . '/' . $viewName . '.php') ;
			$view->render();
		}

		protected function mail($to, $subject, $body)
		{
			if(key_exists('default-layout', App::instance()->getConfig('app')))
			{
				$username = App::instance()->getConfig('app')['admin-email'];
				
				$mail = new PHPMailer(true);

				try {
					//Server settings
					$mail->isSMTP();                                            //Send using SMTP
					$mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
					$mail->SMTPAuth   = true;                                   //Enable SMTP authentication
					$mail->Username   = $username;                     //SMTP username
					$mail->Password   = 'aoluscjoxocfzeqs';                               //SMTP password
					$mail->SMTPSecure = 'ssl';
					$mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

					$mail->setFrom($username, 'Mailer');
					$mail->addAddress($to, 'JCA');     //Add a recipient


					//Content
					$mail->isHTML(true);                                  //Set email format to HTML
					$mail->Subject = $subject;
					$mail->Body    = $body;

					$mail->send();
					//echo 'Message has been sent';
				} catch (Exception $e) {
					//echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
				}
			}
		}

		/**
		 * Render a view
		 * @method render
		 * @param  string  $viewName  View name
		 * @return
		 */
		protected function render($viewName, $model = [])
		{
			// Controller folder
			/*$words = preg_split('@((?<=.)(?=[[:upper:]][[:lower:]])|(?<=[[:lower:]])(?=[[:upper:]]))@', $this->getEntityName());
			$folder_name = implode('-', array_map(function($item)
			{
				return strtolower($item);
			}, $words));

			// View render
			$viewPath = $folder_name . '/' . $viewName . '.php';
			$view = new View($viewPath);
			$view->setModel($model);
			$view->render();*/

			$view = new View($this->buildPath($viewName));
			$view->setLayout('close');
			$view->setModel($model);
			$view->render();
		}

		/**
		 * Build a mail and return its HTML content
		 * @param  string $template Mail template name
		 * @param  string $layout   Mail layout name
		 * @return string           Mail HTML content
		 */
		protected function buildMail($template, $_model, $layout = 'main')
		{
			global $model;
			$model = $_model;

			// Content
			ob_start();
			//$message = $model->body;
			require '../mail/' . $template . '.php';
			$content = ob_get_clean();

			// Layout
			ob_start();
			require '../mail/layouts/' . $layout . '.php';
			return ob_get_clean();
		}

		/**
		 * Redirect the request
		 * @param  string $route  Route to redirect
		 */
		static protected function redirect($route)
		{
			header('Location: ' . $route, true, 301);
			exit();
		}

		/**
		 * Get the entity name from the controller name
		 * @method getEntityName
		 * @return string  Entity name
		 */
		private function getEntityName()
		{
			$className = get_class($this);
			$simpleClassName = substr(strrchr($className, '\\'), 1);
			$entityName = current(explode('Controller', $simpleClassName));

			$words = preg_split('@((?<=.)(?=[[:upper:]][[:lower:]])|(?<=[[:lower:]])(?=[[:upper:]]))@',$entityName);
			return implode('-', array_map(function($item)
			{
				return strtolower($item);
			}, $words));
		}

		protected function getFolderName()
		{
			// Controller folder
			$words = preg_split('@((?<=.)(?=[[:upper:]][[:lower:]])|(?<=[[:lower:]])(?=[[:upper:]]))@', $this->getEntityName());
			return implode('-', array_map(function($item)
			{
				return strtolower($item);
			}, $words));
		}

		public function buildPath($view_name)
		{
			return '../views/' . $this->getFolderName() . '/' . $view_name . '.php';
		}
	}
?>
