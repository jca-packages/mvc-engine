<?php
	namespace Jca\Engine\State;

	use Jca\Engine\Exceptions\HTTPException;
	use Jca\Engine\MVC\Access;
	use Jca\State\Machines\Processor;
	use phpDocumentor\Reflection\PseudoTypes\LowercaseString;

	/**
	 * Route processor
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class Router extends Processor
	{
		private static string $class_namespace = "App\\Controllers\\";

		protected string $entity_name;

		protected string $table_name;

		protected string $controller_name;

		protected string $view_name;

		protected string $method_name;

		/**
		 * Router constructor
		 * @method __construct
		 */
		public function __construct($url)
		{
			parent::__construct(new ControllerState($this), $url);
		}

		protected function init($input) : array
		{
			$uri = parse_url($input, PHP_URL_PATH);
			$uri = substr($uri, 1);
			return explode('/', $uri);
		}

		public function shift()
        {
			return array_shift($this->stack);
        }

		/**
		 * Check view accessibility
		 *
		 * @method canAccess
		 * @param  string    $viewName view to check
		 * @return boolean   View accessibility
		 */
		public function canAccess($controller, $method_name)
		{
			// Class
			$class = new \ReflectionClass($controller);
			
			// Method
			$methods = $class->getMethods();
			$method = current(array_filter($methods, function($item) use($method_name)
			{
				return $item->name == $method_name;
			}));

			// Attribute
			$attributes = $method->getAttributes();
			$attribute = current(array_filter($attributes, function($item) use($method_name)
			{
				return $item->getName() == "Jca\\Engine\\MVC\\Access";
			}));

			if(!$attribute)
				throw new HTTPException(403);

			// Right
			$right = $attribute->newInstance()->right;
			if($right == Access::ACCESS_GUEST)
				return true;
			if($right == Access::ACCESS_ADMIN && key_exists('user', $_SESSION))
				return true;

			return false;
		}

		/**
		 * [request description]
		 * @method request
		 * @return [type]  [description]
		 */
		public function run()
		{
			$method_name = strtolower($_SERVER['REQUEST_METHOD']);
			$method_name .= ucfirst($this->view_name);

			// Class path
			$class_path = "Jca\\Engine\\default\\Controllers\\GenericController";
			if( class_exists(self::$class_namespace.$this->controller_name) &&
			   method_exists(self::$class_namespace.$this->controller_name, $method_name) )
				$class_path = self::$class_namespace.$this->controller_name;

			// Method
			if( method_exists($class_path, $method_name) )
			{
				$controller = new $class_path();
				if(!$this->canAccess($class_path, $method_name))
					throw new HTTPException(403);
				$controller->$method_name();
			}
		}

		public function setEntityName($entity_name){ $this->entity_name = $entity_name; }
		public function setTableName($table_name){ $this->table_name = $table_name; }
		public function setControllerName($controller_name){ $this->controller_name = $controller_name; }
		public function setViewName($view_name){ $this->view_name = $view_name; }
		public function setMethodName($method_name){ $this->method_name = $method_name; }
		public function getEntityName(){ return $this->entity_name; }
		public function getTableName(){ return $this->table_name; }
		public function getControllerName(){ return $this->controller_name; }
		public function getViewName(){ return $this->view_name; }
		public function getMethodName(){ return $this->method_name; }
	}
?>
