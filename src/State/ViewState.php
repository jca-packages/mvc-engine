<?php
	namespace Jca\Engine\State;

	use Jca\Engine\App;

	/**
	 * View state
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class ViewState extends RouteState
	{
		/**
		 * Determine controller class and check its existance
		 * @method handle
		 * @param  Context $context  Context calling this state
		 */
		public function process()
		{
			// View name
			$view_name = App::instance()->getConfig('app')['default_view_name'];
			$part = $this->context->unstack();
			if(!empty($part))
				$view_name = strtolower($part);
			$this->context->setViewName($view_name);

			// Action name
			$method_name = 'goto' . implode('', array_map(function($item)
			{
				return ucfirst($item);
			}, explode('-', $view_name)));
			$this->context->setMethodName($method_name);

			/*if(method_exists($classPath, $methodName))
				return;

			if(!Controller::$auto_mode)
				throw new HTTPException(404);*/

			// Auto view 
			/*$path = '../views/' . strtolower($this->context->getOutput()->controllerName) . '/' . $viewName;
			$default_path = '../views/_default/' . $viewName;
			$framework_path = __DIR__ . '/../views/' . $viewName;

			if(file_exists($path . '.php'))
			{
				$this->context->getOutput()->viewPath = $path;
				return;
			}				
			if(file_exists($default_path . '.php'))
			{
				$this->context->getOutput()->viewPath = $default_path;
				return;
			}
			if(file_exists($framework_path . '.php'))
			{
				$this->context->getOutput()->viewPath = $framework_path;
				return;
			}
			
			throw new HTTPException(404);*/

			// Next state (View)
			$this->context->setState(null);
		}
	}
?>
