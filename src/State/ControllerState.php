<?php
	namespace Jca\Engine\State;

	use Jca\Engine\App;
	use Jca\Engine\Exceptions\HTTPException;

	/**
	 * Controller state
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class ControllerState extends RouteState
	{
		/**
		 * Determine controller class and check its existance
		 * @method handle
		 * @param  Context $context  Context calling this state
		 */
		public function process()
		{
			// Entity name
			$config = App::instance()->getConfig('app');
			if(!array_key_exists('default_entity_name', $config))
				throw new HTTPException(404);

			$entity_name = App::instance()->getConfig('app')['default_entity_name'];
			$part = $this->context->unstack();
			if(!empty($part))
				$entity_name = $part;
			$this->context->setEntityName($entity_name);

			// Table name
			$this->context->setTableName(str_replace('-', '_', $entity_name));

			// Controller name
			$controllerName = implode('', array_map(function($item)
			{
				return ucfirst($item);
			}, explode('-', $entity_name)));
			$this->context->setControllerName($controllerName . 'Controller');

			// Controller class name
			/*$class_path = "Jca\Engine\default\Controllers";
			$controllerClassName = implode('', array_map(function($item)
			{
				return ucfirst($item);
			}, explode('_', $controllerName)));
			$controllerClassName = ucfirst($controllerClassName) . 'Controller';

			// File existance check
			$cwd = getcwd();
			$path = '../controllers/' . $controllerClassName . '.php';
			if(!file_exists($path) && !Controller::$auto_mode)
			{
				$this->context->setHTTPCode(404);
				return;
			}

			// Class existance check
			$classPath = '\\controllers\\' . $controllerClassName;
			if(!class_exists($classPath) && !Controller::$auto_mode)
			{
				$this->context->setHTTPCode(500);
				return;

			// Output write
			$this->context->controllerName = $controllerName;
			$this->context->controllerClassName = $controllerClassName;
			$this->context->viewFolder = '../views/' . $this->part;*/

			// Next state (View)
			$this->context->setState(new ViewState($this->context));
		}
	}
?>
