<?php
	namespace Jca\Engine\State;

	use Jca\State\ConcreteState;

	/**
	 * Base state for URL route handling
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class RouteState extends ConcreteState
	{
		/**
		 * Part of the state
		 * @var string
		 */
		protected $part;

		/**
		 * Read the state part
		 * @method handle
		 * @param  Context  $context  Context calling this state
		 */
		public function process()
		{
			$this->part = $this->context->shift();
		}
	}
?>
