<?php
	namespace Jca\Engine;

	use Jca\Automodel\AutoModel;
	use Jca\Engine\Exceptions\HTTPException;
	use Jca\Engine\State\Router;
	use Jca\Hydra\Managers\DBManager;

	/**
	 * App description class
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class App
	{
		/**
		 * Singleton instance
		 * @var App
		 */
		private static $_instance;

		/**
		 * Application router
		 * @var Router
		 */
		private $router;

		/**
		 * Application config
		 * @var array
		 */
		private $config = [];

		private $user;

		/**
		 * Singleton constructor
		 * @method __construct
		 */
		public function __construct()
		{
			$this->router = new Router($_SERVER["REQUEST_URI"]);
		}

		/**
		 * Get singleton instance
		 * @method instance
		 * @return App  Singleton instance
		 */
		public static function instance()
		{
			if(self::$_instance == null)
			{
				Autoloader::register('../controllers');
				Autoloader::register('../managers');
				Autoloader::register('../models');
				self::$_instance = new App();
			}
			return self::$_instance;
		}

		/**
		 * Run the app
		 * @method run
		 */
		public function run()
		{
			session_start();

			try
			{
				// Config
				$files = glob('../config/*.{json}', GLOB_BRACE);
				foreach($files as $file)
					$this->config[basename($file, '.json')] = json_decode(file_get_contents($file), true);

				// DB Engine
				if(array_key_exists('db', $this->config))
					AutoModel::setExecutor(DBManager::instance()->getExecutor());

				$this->router->process();
				$this->router->run();
			}
			catch(HTTPException $e)
			{
				http_response_code($e->getHTTPCode());
				if($e->getHTTPCode() == 403 && !key_exists('user', $_SESSION))
				{
					header('Location: ' .  '/authentification/login');
					return;
				}
				if(key_exists('user', $_SESSION))
					http_response_code(200);
			}
			catch(\mysqli_sql_exception $e)
			{
				http_response_code(500);
			}
		}

		/**
		 * Get server root
		 * @method root
		 * @return string  Server root
		 */
		public static function root()
		{
			return (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		}

		public static function generateRandomString()
		{
			return random_bytes(32);
		}

		/**
		 * Get the app config
		 * @param string $key Configuraiton key
		 * @return array App config
		 */
		public function getConfig($key)
		{
			if(array_key_exists($key, $this->config))
				return $this->config[$key];
			return [];
		}

		public static function title()
		{
			return App::instance()->getConfig('app')['title'];
		}

		public static function getUploadFolder()
		{
			$env = App::instance()->getConfig('app')['env'];
			if( $env == 'prod')
				return '../../../library/';
			return './upload/';
		}

		public static function getPicFolder($path)
		{
			$env = App::instance()->getConfig('app')['env'];
			if( $env == 'prod')
				return '../../../library/';
			$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
			return $root . $path;
		}

		public function getRouter(){ return $this->router; }
	}
?>
