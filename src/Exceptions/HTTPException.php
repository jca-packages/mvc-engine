<?php
    namespace Jca\Engine\Exceptions;

    use Exception;

    class HTTPException extends Exception
    {
        private $http_code;

        public function __construct($http_code)
        {
            $this->http_code = $http_code;
        }

        public function getHTTPCode()
        {
            return $this->http_code;
        }
    }
?>