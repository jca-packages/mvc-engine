<?php
	namespace Jca\Engine;

	/**
	 * Autoloader for controller classes
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class Autoloader
	{
		public static function register($path)
		{
			spl_autoload_register(function ($class) use ($path)
			{
				$class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
				$class = explode(DIRECTORY_SEPARATOR, $class);
				$class = end($class);
				$file = $class . '.php';

				if (file_exists($path . '/' . $file))
				{
					require_once $path . '/' . $file;
					return true;
				}
				return false;
			});
		}
	}
?>
