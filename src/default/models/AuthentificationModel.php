<?php
	namespace Jca\Engine\Default\Models;

	use Jca\Automodel\Model;
	use Jca\Engine\App;

	class AuthentificationModel extends Model
	{
        public function __construct(public string $login = "",
                                    public string $password = "") {
			parent::__construct();
		}

		public function save()
		{
			$login = App::instance()->getConfig('app')['login'];
			$password = App::instance()->getConfig('app')['password'];

			if($this->login == $login && $this->password == $password)
				return true;
			return false;
		}
	}
?>
