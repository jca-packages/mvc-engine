<?
	use Jca\Engine\App;
?>

<?php
	 $entity_name = App::instance()->getRouter()->getEntityName();;
?>

<div class="w-f d-f jc-c">
	<table>
		<?php foreach($this->model->getColumns() as $field => $row): ?>
		<tr>
			<td>
				<?= $field ?>
			</td>
			<td>
				<?php
					if(str_contains($field, 'date'))
						echo date('Y-m-d', strtotime($this->model->$field));
					else if(gettype($this->model->$field) == 'object')
						echo "<a class='button link' href='/{$this->model->$field->getTableName()}/view?id={$this->model->$field->id}'>{$this->model->$field->id}</a>";
					else
						echo $this->model->$field;
				?>
			</td>
		</tr>
		<?php endforeach; ?>
		
		<?php if(key_exists('user', $_SESSION)): ?>
		<tr class="h-g-6">
			<td>
				Update
			</td>
			<td>
				<a class="button link bg-c-p c-a frame-auto pl-1 pr-1" href="/<?= $entity_name ?>/update?id=<?= $this->model->id ?>">update</a>
			</td>
		</tr>
		<tr class="h-g-6">
			<td>
				Delete
			</td>
			<td>
				<a class="button link bg-c-p c-a frame-auto pl-1 pr-1" href="/<?= $entity_name ?>/delete?id=<?= $this->model->id ?>">delete</a>
			</td>
		</tr>
		<?php endif; ?>
	</table>
</div>
