<?
	use Jca\Engine\App;
?>

<?php
	 $entity_name = App::instance()->getRouter()->getEntityName();
?>

<div class="w-f d-f ai-c o-v">
<?php if(empty($this->model)) { ?>
	No data
<?php } else { ?>
	<table>
		<tr>
			<?php 
				$fields = current($this->model)->getColumns(); 
				foreach($fields as $field => $row): ?>
			<th>
				<?= $field ?>
			</th>
			<?php endforeach ?>
			<th></th>
			<th></th>
		</tr>
	<?php foreach($this->model as $entity) { ?>
		<tr>
			<?php foreach($fields as $field => $row): ?>
			<td>
				<?php
					
					$type = $row['Type'];

					//echo $type;
					if( $type == 'Jca\Automodel\AutoModel')
						echo "<a class='button link' href = '/{$value->getTableName()}/view?id={$value->id}\'>{$value->id}</a>";
					else if(str_contains($field, 'date'))
						echo date('Y-m-d', strtotime($entity->$field));
					else
						echo $entity->$field;
				?>
			</td>
			<?php endforeach ?>

			<td>
				<a class="button link bg-c-p c-a w-g-8 frame-auto" href="/<?= $entity_name ?>/view?id=<?= $entity->id ?>">view</a>
			</td>
			<td>
				<a class="button link bg-c-p c-a w-g-8 frame-auto" href="/<?= $entity_name ?>/delete?id=<?= $entity->id ?>">delete</a>
			</td>
		</tr>
	<?php } ?>
	</table>
<?php } ?>
	<a class="button bg-c-p c-a w-g-8 frame-auto" href="/<?= $entity_name ?>/create">create</a>
</div>
