<?
	use Jca\Automodel\AutoModel;
	use Jca\Automodel\Utils\NameUtils;
	use Jca\Hydra\Form\Form;
?>

<div class="container pa-1">
	<?php $form = Form::begin($this->model, 'create') ?>

	<?php
		foreach($this->model->getColumns() as $field => $row)
		{
			if($row['Extra'] == 'auto_increment')
				continue;

			if( $row['Key'] == 'MUL')
			{
				$methodName = "find" . NameUtils::tableNameToClassName($row['REFERENCED_TABLE_NAME']);
				$data = Automodel::$methodName();
				
				echo $form->select($field, $data);
			}
			else if( $row['Type'] == 'text')
				echo $form->richText($field);
			else
				echo $form->text($field);
		}

		
		

		echo $form->submit();
	?>

	<?php $form->end() ?>
</div>