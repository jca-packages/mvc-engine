<?php
	namespace Jca\Engine\default\Controllers;

	use Jca\Automodel\Automodel;
	use Jca\Engine\App;
	use Jca\Engine\MVC\Access;
	use Jca\Engine\MVC\Controller;
	use Jca\Engine\MVC\View;

	/**
	 * Controller class
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class GenericController extends Controller
	{
		#[Access(right: Access::ACCESS_ADMIN)]
		public function getCreate()
		{
			$model = new Automodel(App::instance()->getRouter()->getTableName());

			$path = 'views/' . App::instance()->getRouter()->getEntityName() . '/create.php';
			
			$view = new View(file_exists($path) ? $path : Controller::getDefaultPath('create'));
			$view->setModel($model);
			$view->render();
		}

		#[Access(right: Access::ACCESS_ADMIN)]
		public function postCreate()
		{
			$model = new Automodel(App::instance()->getRouter()->getTableName());

			// Model fill
			$post = $_POST;
			$model->fill($post);

			if($model->validate($post) && $model->save())
			{
				header('Location: ' .  'view?id=' . $model->id , true, 301);
				exit();
			}

			$path = 'views/' . App::instance()->getRouter()->getEntityName() . '/create.php';

			$view = new View(file_exists($path) ? $path : Controller::getDefaultPath('create'));
			$view->setModel($model);
			$view->render();
		}

		#[Access(right: Access::ACCESS_ADMIN)]
		public function getUpdate()
		{
			$id = $_GET['id'];
			$method_name = 'find' . ucfirst(App::instance()->getRouter()->getTableName()) . 'ById';
			$model = Automodel::$method_name($id)[0];

			$path = 'views/' . App::instance()->getRouter()->getEntityName() . '/update.php';
			
			$view = new View(file_exists($path) ? $path : Controller::getDefaultPath('update'));
			$view->setModel($model);
			$view->render();
		}

		#[Access(right: Access::ACCESS_ADMIN)]
		public function postUpdate()
		{
			$id = $_POST['id'];
			
			$method_name = 'find' . ucfirst(App::instance()->getRouter()->getTableName()) . 'ById';
			$model = Automodel::$method_name($id)[0];

			// Model fill
			$post = $_POST;
			$model->fill($post);

			if($model->validate($post) && $model->save())
			{
				header('Location: ' .  'view?id=' . $model->id , true, 301);
				exit();
			}

			$path = 'views/' . App::instance()->getRouter()->getEntityName() . '/create.php';

			$view = new View(file_exists($path) ? $path : Controller::getDefaultPath('create'));
			$view->setModel($model);
			$view->render();
		}

		#[Access(right: Access::ACCESS_GUEST)]
        public function getIndex()
		{
			$method_name = 'find' . ucfirst(App::instance()->getRouter()->getTableName());

			$model = Automodel::$method_name();
			if(!is_array($model))
				$model = [$model];

			$path = 'views/' . App::instance()->getRouter()->getEntityName() . '/index.php';
			
			$view = new View(file_exists($path) ? $path : Controller::getDefaultPath('index'));
			$view->setModel($model);
			$view->render();
		}

		#[Access(right: Access::ACCESS_GUEST)]
		public function getView()
		{
			$id = $_GET['id'];

			$entity_name = App::instance()->getRouter()->getEntityName();

			$methodName = 'find' . ucfirst(App::instance()->getRouter()->getTableName()) . 'ById';
			$model = Automodel::$methodName($id)[0];

			$path = 'views/' . $entity_name . '/view.php';
			
			$view = new View(file_exists($path) ? $path : Controller::getDefaultPath('view'));
			$view->setModel($model);
			$view->render();
		}

		#[Access(right: Access::ACCESS_ADMIN)]
		public function getDelete()
		{
			$id = $_GET['id'];
			$method_name = 'find' . ucfirst(App::instance()->getRouter()->getTableName()) . 'ById';
			$model = Automodel::$method_name($id)[0];
			$model->delete();

			self::redirect("/" . App::instance()->getRouter()->getEntityName() . "/index");
		}

		public static function get()
		{
			$view = new View($entityName . '/' . $viewName . '.php') ;
			$view->render();
		}
	}
?>
